//: [Previous](@previous) : Introduction
/*: 
# Simple Values
You can work think of values broadly in 2 categories:
1. Values that change, also known as Variables
2. Values that don't change, also known as Constants

Swift lets you deal with these types of values.
* Use `let` to declare a new constant 
* Use `var` to declare a new variable

Things to know:
* You cannot change the value of a constant
* You cannot change the value of a variable to a different type

*/

//: ## Declaring a constant

let language = "Swift"
let appName = "MeTube"

//: ## Declaring a variable

let pi = 3.142

var name = "Jason"
var videoTitle = "My Swift Video"
videoTitle = "Swift Variables Video"

//: [Next](@next) Naming Conventions
