/*:
# Swift 101 Walkthrough
> Welcome to AppLab's Swift 101 Walkthrough. We prepared this walkthough to help developers coming from other languages quickly familiarize yourselves with Swift 2.0, as a basis to start learning to develop mobile apps with Swift.

Remember that Swift is just a language -- it's an awesome language -- but at its core it's not all that different from programming languages you know. It has its own syntax of course, but still deals with values, control flow, functions, classes, methods and more. Once you can map the syntax with against what you understand of programming languages, you're on track to learn the intricacies of the language and platform, but for now, Swift language is the first step.
*/

var str = "Hello, Jason!"
var appName = "MeTube"

/*:
[Next](@next) Simple Values
*/
