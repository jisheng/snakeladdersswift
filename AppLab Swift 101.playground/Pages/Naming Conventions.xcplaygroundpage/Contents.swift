//: [Previous](@previous) Simple Values
/*:
# Naming Conventions

While it doesn't matter to the program how you name your constants and variables, it matters a LOT to you and me as humans that we do naming correctly from the start. Being good at naming will help you understand your own code, as well as keep a clear mental model of what the program is doing. Your teammates will also love you more for it.

Here are some things to remember when naming your constants and variables:

* Be clear about what the variable is for
* Be concise, but don't use shrtfrms
* CAPITAL_NAMES are for static constants
* lowercase for local constants and variables
* camelCasedName if your name needs to be more than one word
* Use the singular form for a single value
* Use the plural form for collection of values
* Avoid ambiguous naming, clarify the variable type if needed

*/
import UIKit

let pi = 3.142

//let categories = ["Music", "Trailers", "Funny", "Tutorials"]

let SITE_NAME = "MeTube.com"
var visitCount = 32
visitCount++

let firstName = "Jason"

let categories = ["Music", "Trailers", "Funny", "Tutorials"]
let video = "Everest Trailer"


var title = video
var titleLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 130, height: 20))
titleLabel.text = title

//: [Next](@next) Data Types
