//: [Previous](@previous)
/*:
# Assignment: Snake & Ladders
Now that you've got some idea how Swift looks like, it's time for you to get your hands dirty doing the first Swift assignment.

Build a simple Snake & Ladders game. You'll need to have some game setup function, which keeps a board of 100 squares. Some of the squares are "Snakes", meaning that a player who lands on the square will fall back a number of positions. Some of the squares are "Ladders", where a player who lands there will go forward a number of positions. The goal, is to be the first to reach the 100th square. You can set the number of players as a variable, anything between 2 to 5 makes a good number.

The main loop will basically roll the dice for each player, and print out the their current position, rolled dice, new position, and effect of landing on a snake or ladder.

No user input is expected for this simple assignment. The program just has to keep on running until one player reaches the goal.
*/
import Foundation

// Initial variables (just to get you started)
class Player {
    var playerLocation: Int
    var playerName: String
    
    init(name: String) {
        self.playerLocation = 0
        self.playerName = name
    }
}

class SnakeLadder {
    // class SnakeLadder
    var head: Int
    var tail: Int
    
    init(head: Int, tail: Int){
        self.head = head
        self.tail = tail
    }
}

let BOARD_SIZE = 1000
var players: [Player] = [
    Player(name: "AAA"),
    Player(name: "BBB"),
    Player(name: "CCC"),
    Player(name: "DDD")
]

var activationZone: [Int] = []
var snakeLadders: [SnakeLadder] = []

// Functions
func createSnakeLadder() {
    var currentIteration = Int(rand()) % BOARD_SIZE + 1
    var changes = (Int(rand()) % (BOARD_SIZE / 3)) - (BOARD_SIZE / 6)
    while(true){
        if (currentIteration + changes <= 0 || currentIteration + changes >= BOARD_SIZE ){
            changes = (Int(rand()) % (BOARD_SIZE / 3)) - (BOARD_SIZE / 6)
        } else if (activationZone.contains(currentIteration)) {
            currentIteration = Int(rand()) % BOARD_SIZE + 1
        } else {
            snakeLadders.append(SnakeLadder(head: currentIteration, tail: currentIteration + changes))
            activationZone.append(currentIteration)
            break
        }
    }
}

func boardSetup() {
    for var iterate = 0; iterate < BOARD_SIZE / 5; ++iterate {
        createSnakeLadder()
        if (snakeLadders[iterate].head > snakeLadders[iterate].tail) {
            print("Snake created : Head = \(snakeLadders[iterate].head) & Tail = \(snakeLadders[iterate].tail)")
        } else {
            print("Ladder created : Start = \(snakeLadders[iterate].head) & End = \(snakeLadders[iterate].tail)")
        }
    }
    print("\(activationZone)")
}

func getSnakeLadder(location: Int) -> Int {
    for snakeLadder in snakeLadders {
        if(snakeLadder.head == location){
            return snakeLadder.tail
        }
    }
    return -1
}

func rollDice() -> Int {
    return Int(rand()) % 6 + 1
}

// Main
let time = UInt32(NSDate().timeIntervalSinceReferenceDate)
srand(time)
boardSetup()
var ended:Bool = false
var turnNumber = 1

while (!ended) {
    print("Turn : \(turnNumber)")
    for currentPlayer in players {
        print("Player \(currentPlayer.playerName) currently at \(currentPlayer.playerLocation)")
        
        let rolledDice = rollDice()
        var newPostion:Int = 0
        print("Dice rolled. Got \(rolledDice)")
        
        if(currentPlayer.playerLocation + rolledDice >= BOARD_SIZE){
            currentPlayer.playerLocation = BOARD_SIZE
            print("Player \(currentPlayer.playerName) currently at \(currentPlayer.playerLocation)")
            print("Player \(currentPlayer.playerName) won")
            ended = true
            break
        } else {
            var latestPosition = getSnakeLadder(currentPlayer.playerLocation + rolledDice)
            if(latestPosition != -1) {
                newPostion = latestPosition
            } else {
                newPostion = currentPlayer.playerLocation + rolledDice
            }
        }
        
        if(currentPlayer.playerLocation + rolledDice > newPostion) {
            currentPlayer.playerLocation = newPostion
            print("Player \(currentPlayer.playerName) got a snake and currently at \(currentPlayer.playerLocation)")
        } else if(currentPlayer.playerLocation + rolledDice < newPostion) {
            currentPlayer.playerLocation = newPostion
            print("Player \(currentPlayer.playerName) got a ladder and currently at \(currentPlayer.playerLocation)")
        } else {
            currentPlayer.playerLocation = newPostion
            print("Player \(currentPlayer.playerName) currently at \(currentPlayer.playerLocation)")
        }
        print("")
    }
    turnNumber += 1
}
