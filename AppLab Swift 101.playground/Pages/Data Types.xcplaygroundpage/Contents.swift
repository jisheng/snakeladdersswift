//: [Previous](@previous) : Naming Conventions
/*:
# Data Types
Swift has a complete set of its own fundamental types, similar to what C or Objective C has, the primitive Java data types.
These include:
- Int
- Double and Float
- Bool
- String

Things to remember about Swift data types:
* You cannot mix different data types
* A variable of a specific type cannot be changed to another
* Use `\()` to include values and expressions into a string
* Values cannot be nil (empty) at any point in time, unless they are `Optional`
*/

let language = "Swift"
let siteName = "MeTube"

let videoLength = 10
let introLength = 3.5

let videoIsDone = true

// var totalLength = videoLength + introLength

/*: ## Optionals
If you have some variable that may be `nil`, you need to specifically state it as such.
Encountering unexpected `nil` values have been a major cause of crashes in the past.

Optionals is part of Swift's safety checks, use it to your benefit!
* Don't make everything optional
* If a value could be  `nil`, do try to handle those cases
* Get used to writing code that looks "funny", like `if let name = name` (Optional binding)
* Be very very careful when using implicitly unwrapped optionals
*/
import Foundation

var randomNumber = rand()
var firstName = "Jason"
var middleName: String?
var lastName = "Khong"

//middleName = " WS "
//var name = firstName + middleName! + lastName

if let middleName = middleName {
    var name = firstName + middleName + lastName
} else {
    var name = firstName + lastName
}


//: [Next](@next) Type Inference
