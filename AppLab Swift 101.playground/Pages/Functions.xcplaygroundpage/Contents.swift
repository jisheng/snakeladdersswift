//: [Previous](@previous) Control Flow
/*:
# Functions

You can and should organize your code into smaller, reusable units. Even in Playground, you can write functions using the `func` keyword.

Functions are first class types in Swift, so you can use them as parameters or returned values too.
*/

import Foundation

func doubleOrNothing(score: Int) -> Int {
    let randomMood: Int = Int(random()) % 8
    if randomMood > 5 {
        return score * 2
    } else {
        return 0
    }
}

var testScore: Int = 33
testScore = doubleOrNothing(testScore)
testScore = doubleOrNothing(testScore)
testScore = doubleOrNothing(testScore)

func showVideo(video: String) {
    // Start playing video
    print("Start playing: \(video)")
}

func randomVideo() -> String {
    // Find a random video...
    return "Random Video"
}


//showVideo("First Video")

showVideo(randomVideo())
//: [Next](@next) Assignment
