//: [Previous](@previous)
/*:

# Collections

Swift provides some handy data types to deal with collections:
* Array: Sorted list of values
* Set: Unsorted list of unique values
* Dictionary: Key - value named pairs of values

Choosing the correct type of collection will ease the work needed when you code later.

Some things to remember:
* A collection is always for a group of values of the same type
* Use superclasses (AnyObject) or Protocols if dealing with similar values but different types

Demo
- Declaring an array
- Adding values
- Removing values
- Joining 2 sets of collections

*/

import Foundation

var colorNames: [String] = ["Red", "Green", "Blue"]
var fruitNames: [String] = ["Apple", "Banana", "Orange"]
var scores: [Int] = [42, 98, 73, 20]

var combined = colorNames + fruitNames

colorNames.append("Yellow")
colorNames.removeAtIndex(0)
colorNames[2]
print("Color array contains: \(colorNames)")
//: [Next](@next) Control Flow
